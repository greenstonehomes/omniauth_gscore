module OmniAuth
  module Strategies
    class Gscore < OmniAuth::Strategies::OAuth2
      option :name, :gscore

      option :client_options, {
        :site => "http://core.greenstonehomes.com",
        :authorize_url => "oauth/authorize",
        :token_url => "oauth/token"
      }

      option :sub_uri, ''

      uid { raw_info["user"]["id"] }

      info do
        {
          email: raw_info["user"]["email"],
          username: raw_info["user"]["username"],
          name: raw_info["user"]["name"]
          # and anything else you want to return to your API consumers
        }
      end

      extra do
        {
          raw_info: raw_info,
          roles: raw_info["user"]["roles"]
        }
      end

      def raw_info
        @raw_info ||= access_token.get('api/v4/me.json').parsed
      end
      
    end
  end
end

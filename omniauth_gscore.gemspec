$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "omniauth_gscore/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "omniauth_gscore"
  s.version     = OmniauthGscore::VERSION
  s.authors     = ["Nik Petersen"]
  s.email       = ["nik@petersendata.net"]
  s.homepage    = "http://www.greenstonehomes.com"
  s.summary     = "An oauth2 omniauth strategy for Greenstone's Core system"
  s.description = "An oauth2 omniauth strategy for Greenstone's Core system"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "omniauth"
  s.add_dependency "omniauth-oauth2"

end
